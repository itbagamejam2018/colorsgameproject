﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
	public Camera uiCam;

	void Start()
	{
		var rt = transform as RectTransform;
		var world = rt.position;
		var viewInOtherCamera = uiCam.WorldToViewportPoint(world);
		var worldFromView = Camera.main.ViewportToWorldPoint(viewInOtherCamera);
		print(worldFromView);
	}
}
