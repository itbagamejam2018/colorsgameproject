﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Coords
{
	public int x;
	public int y;

	public void Set(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}

public class GameController : MonoBehaviour
{
	[Header("UI")]
	public Image currentColorUI;
	public Image oppositeColorUI;


	public ColorData moveLeftColor;
	public ColorData moveRightColor;
	public ColorData moveUpColor;
	public ColorData moveDownColor;

	[Header("Player")]
	public PlayerController player;
    
	public static GameController Instance;

    public GameObject[] Levels;

    public RuntimeAnimatorController animatorController;
    public AnimationClip [] EnemiesAnimations;

    private int ActualLevel = 0;

	public UnityEvent OnTick = new UnityEvent();
	private float _accumulatedTime;
	public float turnTime = 0.5f;

	private void Awake()
	{
		Instance = this;
        InitLevelsFlow();
		GetComponent<sonidos>().sound();
    }

	private void OnDestroy()
	{
		Instance = null;
		OnTick.RemoveAllListeners();
	}

	private void Update()
	{
		if (Input.GetKeyDown(moveLeftColor.DebugKey)) SetColor(moveLeftColor);
		if (Input.GetKeyDown(moveRightColor.DebugKey)) SetColor(moveRightColor);
		if (Input.GetKeyDown(moveUpColor.DebugKey)) SetColor(moveUpColor);
		if (Input.GetKeyDown(moveDownColor.DebugKey)) SetColor(moveDownColor);

		Tick();
	}

	private void Tick()
	{
		_accumulatedTime += Time.deltaTime;

		if(_accumulatedTime >= turnTime)
		{
			// TURN
			if (OnTick != null) OnTick.Invoke();
			_accumulatedTime = 0;
		}
	}

	// Llamar desde los botones
	// public static Instance de GameController quizás?
	public void SetColor(ColorData color)
	{
		if (color == null)
		{
			currentColorUI.color = Color.white;
			oppositeColorUI.color = Color.black;
			player.ColorData = null;
			Move(0,0);
			return;
		}

		currentColorUI.color = color.VisualColor;
		oppositeColorUI.color = color.Complementary.VisualColor;
		player.ColorData = color;

		if (color == moveLeftColor) Move(-1, 0);
		else if (color == moveRightColor) Move(+1, 0);
		else if (color == moveDownColor) Move(0, -1);
		else if (color == moveUpColor) Move(0, +1);
		else
		{
			Move(0, 0);
		}
	}

	private void Move(int x, int y)
	{
		// Mover _currentPlayerCoords +x +y, 
		// o usar x e y como multipliers de velocidad en cada eje
		// si es algo continuo y no por turnos
		player.Move(x, y);
	}

    
    public void GoToMainMenu() {
        SceneManager.LoadScene(1);
    }

    public void InitLevelsFlow()
    {
        for (int i = 0; i < Levels.Length; i++) {
            if (i == 0)
                Levels[i].SetActive(true);
            else
                Levels[i].SetActive(false);
        }

    }

	public void ResetLevel()
	{
		SetColor(null);
		player.ResetPlayer();
	}

	public void NextLevel()
	{
		if(ActualLevel +1 >= Levels.Length)
		{
			Win();
			return;
		}

		Levels[ActualLevel].SetActive(false);
		Levels[++ActualLevel].SetActive(true);
		SetColor(null);
	}

	private void Win()
	{
		print("WIIIIIINNNNNN");
	}
}
