﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GenericTile : MonoBehaviour {

   
    public TileType tileType;
    public ColorTile TileColor;
    public ColorData [] _TileColor;

   
    public enum ColorTile {
        white,
        NONE,
        yellow,
        green,
        red,
        blue,
        black

    }

    public enum TileType {

        Piso,
        Pared,
        Pinchos,
        Disparador,
        Sierra,
        Inicio,
        Fin,
        SierraLateral
    }
       
	// Use this for initialization
	void Start () {
        SetColor();

		switch (tileType) {
			case TileType.Pinchos:
				gameObject.AddComponent<Pinchos>();
				break;
			case TileType.Sierra:
				gameObject.AddComponent<Saw>();
				break;
			case TileType.SierraLateral:
				var saw = gameObject.AddComponent<Saw>();
				saw.Axis = 0;
				break;
			case TileType.Disparador:
				break;
		}
    }


    /*
    private void addAnimator(AnimationClip clip) {

        this.gameObject.AddComponent<Animator>();
        gameObject.GetComponent<Animator>().runtimeAnimatorController = GameController.GetComponent<GameController>().animatorController;
    }
    */
    private void SetColor() {
        this.gameObject.layer = 0;       
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        this.gameObject.layer = 0;

        switch (TileColor) {

            case ColorTile.NONE:
                
              
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                this.gameObject.layer = 2;

                break;
            case ColorTile.yellow:
               
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[1].VisualColor;
            
                break;
            case ColorTile.green:
                
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[2].VisualColor;
                
                break;
            case ColorTile.red:
            
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[3].VisualColor;
                
                break;
            case ColorTile.blue:
            
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[4].VisualColor;
                
                break;
            case ColorTile.white:
             
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[5].VisualColor;
               
                break;
            case ColorTile.black:
               
                this.gameObject.GetComponent<SpriteRenderer>().color = _TileColor[6].VisualColor;
                
                break;
        }
       

    }

   
    private void OnValidate()
    {
        SetColor();
    }

}
