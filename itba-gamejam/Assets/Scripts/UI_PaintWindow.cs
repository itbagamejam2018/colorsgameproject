﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UI_PaintWindow : MonoBehaviour
{
	public List<ColorData> _colors;

    //this script only controls the creation of N buttons
    public Button GenericColorButton;
    public Transform ButtonsContainer;
    public Color colorToTest;
    public List<GameObject> ButtonsInScene = new List<GameObject>();
    public AudioClip[] botonesSonidos;

	// Use this for initialization
	void Start () {
        OnButtonsInitialize();
    }

    int indexSound = 0;
    public void OnButtonsInitialize() {
        
		_colors.ForEach(d =>
		{
			var btn = GameObject.Instantiate(GenericColorButton.gameObject, ButtonsContainer);
           btn.name = "BTN_Color_" + d.name;
            btn.GetComponent<AudioSource>().clip = botonesSonidos[indexSound];
            indexSound++;
           ButtonsInScene.Add(btn);
           btn.GetComponent<GenericButtonColor>().ColorData = d;
		});
    }
}
