﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float turnTime;
    public LayerMask layerMask;
    private Rigidbody2D _rigidbody;
    private Renderer _renderer;
    private float _tileSize = 1;

    private float _accumulatedTime;
    private Coords _currentPlayerCoords;
    private Coords _playerMovement = new Coords();

    private ColorData _colorData;
    private RaycastHit2D[] results = new RaycastHit2D[4];

    public GameObject DeadPopUp;
	private Vector3 initialPos;


    public ColorData ColorData { get { return _colorData; }
        set {
            _colorData = value;
			if(value == null)
			{
				_renderer.material.color = Color.white;

			}else _renderer.material.color = value.VisualColor;
        } }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _renderer = GetComponent<Renderer>();
    }


    private void Start()
    {
        // transform.position = gridContainer.Instance.StartTile.transform.position;
        OnSetInitialPosition();
		GameController.Instance.OnTick.AddListener(Tick);
    }

    public int ActuaLevel = 0;
    private GenericTile[] gt;

    private void OnSetInitialPosition()
    {
		initialPos = transform.position;
    }

	public void Move(int x, int y)
	{
		_playerMovement.Set(x, y);
		//_rigidbody.velocity = new Vector2(speed * x, speed * y);
	}

	private void Update()
	{
		var dir = new Vector3(_playerMovement.x, _playerMovement.y, 0);
		Debug.DrawRay(transform.position + dir * 0.5f, dir, Color.cyan);
	}

	private void Tick()
	{
		var dir = new Vector3(_playerMovement.x, _playerMovement.y, 0);

		if (Physics2D.RaycastNonAlloc(transform.position + dir * 0.5f, dir, results, 0.999f, layerMask) > 0)
			{
				var tile = results[0].collider.GetComponent<GenericTile>();

				if(tile != null)
				{
                    if (tile.tileType == GenericTile.TileType.Pinchos)
                    {
                        Debug.Log("PINCHOS!!");
                        youAreDead();
                    }
                    if (tile.tileType == GenericTile.TileType.Sierra) {
                        Debug.Log("Sierra!!");
                        youAreDead();
                    }
                    if (tile.tileType == GenericTile.TileType.Disparador) {
                        Debug.Log("Mov!!");
                        youAreDead();
                    }
                    if (tile.tileType == GenericTile.TileType.Fin)
                    {
                        Debug.Log("GoToNextLevel");
						NextLevel();
                    }

                    //print(tile.tileType == GenericTile.TileType.Fin ? "------ WIN" : "----- LOSE");
                    return;
				}
			}



			float x = _playerMovement.x * _tileSize;
			float y = _playerMovement.y * _tileSize;

			_rigidbody.position = _rigidbody.position + new Vector2(x, y);
	}

	private void NextLevel()
	{
		GameController.Instance.NextLevel();
		// Reset player
		transform.position = initialPos;
	}

	private void OnStopMovement() {

        Move(0, 0);
    }

    
    private void youAreDead() {

        DeadPopUp.SetActive(true);
    }

    public void ResetPlayer()
    {
        DeadPopUp.SetActive(false);
        transform.position = new Vector2(0, 0);
    }
}
