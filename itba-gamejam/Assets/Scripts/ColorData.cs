﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ColorData : ScriptableObject
{
	public KeyCode DebugKey;
	public Color VisualColor;
	public ColorData Complementary;
}
