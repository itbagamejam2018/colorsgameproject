﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class SplashController : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<VideoPlayer>().loopPointReached += Continue;
	}

    private void Continue(VideoPlayer source)
    {
        source.loopPointReached -= Continue;
        SceneManager.LoadScene(1);
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
