﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class gridContainer : MonoBehaviour {

    public GenericTile tilePrefab;
    public Transform TileParent;
    public GameObject referencePostile;

    private List<GenericTile> tileList = new List<GenericTile>();

    public int totalTiles;
    public int fill;
    public int Row;
    public float Line = 0;

	public GenericTile StartTile;
    public GenericTile EndTile;

	public static gridContainer Instance;

    void Awake ()
    {
        StartTile =  GetComponentsInChildren<GenericTile>().Where(t => t.tileType == GenericTile.TileType.Inicio).First();
        EndTile = GetComponentsInChildren<GenericTile>().Where(t => t.tileType == GenericTile.TileType.Fin).First();
		Instance = this;
    }

	private void OnDestroy()
	{
		Instance = null;
	}

	void Start ()
	{
      //   CreateGrid();
    }
	
    public float Tilesize;
    private Vector2 InitialWorldPosition = new Vector2(5f, 7.9f);

    private void CreateGrid() {
        //InitialWorldPosition = referencePostile.GetComponent<Transform>().position;
        // TileParent.transform.position = new Vector2(InitialWorldPosition.x +10, InitialWorldPosition.y + 10);
        tileList.Clear();
        for (int x = 0; x < fill; x++)
        {
            for (int y = 0; y < Row; y++)
            {
                GameObject tile = GameObject.Instantiate(tilePrefab.gameObject, TileParent.transform);
                tile.gameObject.name = "TileLine_" + x.ToString("D2") + "- " + y.ToString("D2");
                tile.gameObject.GetComponent<Transform>().position = new Vector2(InitialWorldPosition.x + x * Tilesize, InitialWorldPosition.y - y * Tilesize);
                tileList.Add(tile.GetComponent<GenericTile>());

            }

        }

    }
  

}
