﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Saw : MonoBehaviour
{
	private Vector3 InitialPosition = new Vector3();
	private int dir = 1;
	public int Axis = 1;


	private void Start()
	{
		InitialPosition = transform.position;
		this.gameObject.isStatic = false;
		GameController.Instance.OnTick.AddListener(Tick);
	}

	private void Tick()
	{
		var pos = gameObject.transform.position;
		pos[Axis] += 1 * dir;


		if (pos[Axis] <= InitialPosition[Axis] - 1 || pos[Axis] >= InitialPosition[Axis] + 1)
		{
			dir *= -1;
		}

		transform.position = pos;
	}
}
