﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoTO(int next) {

        if(next == 0)
            Application.Quit();
       if (next == 1)
            SceneManager.LoadScene(2);
        if (next == 2)
            SceneManager.LoadScene(3);
        if (next == 3)
            SceneManager.LoadScene(1);
    }
}
