﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericButtonColor : MonoBehaviour
{
	private Button _button;
	private ColorData _colorData;

	public ColorData ColorData {
		get { return _colorData; }
		set {
			_colorData = value;
			OnSetColor(value.VisualColor);
		}
	}

    private Image btnImage; 

    public void OnSetColor(Color color) {
        Debug.Log("Color Setting");
		btnImage = GetComponent<Image>();
		btnImage.color = color;
		_button = GetComponent<Button>();
		_button.onClick.AddListener(SetCurrentColor);
    }

	private void SetCurrentColor()
	{
		GameController.Instance.SetColor(_colorData);
	}
}
