﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinchos : MonoBehaviour
{
	bool _isActive;
	int _tickCount;
	int _activeTurns = 1;
	int _inactiveTurns = 3;
    
	private void Start()
	{
		GameController.Instance.OnTick.AddListener(Tick);
		Set(_isActive);
	}

	private void Tick()
	{
		if(_isActive && _tickCount >= _activeTurns)
		{
			_isActive = false;
			_tickCount = 0;
		}
		else if(!_isActive && _tickCount >= _inactiveTurns)
		{
			_isActive = true;
			_tickCount = 0;
		}

		Set(_isActive);
		_tickCount++;
	}

	private void Set(bool isActive)
	{
		gameObject.SetActive(isActive);
	}
}
